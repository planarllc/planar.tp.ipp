message("Ipp-2017.0.3 in " $$PWD)

IPP_ARCH = x64
contains(QMAKE_HOST.arch, x86):IPP_ARCH = x32

win32:contains(QMAKE_HOST.os, Linux):IPP_ARCH = x32

LIBS += -L$$PWD/$$IPP_ARCH -lipps -lippvm -lippcore
INCLUDEPATH += $$PWD/inc/

unix:!macx:IPP_FILES = $$PWD/$$IPP_ARCH/*.so
win32:IPP_FILES = $$PWD/$$IPP_ARCH/*.dll

IPP_FILES_LIST = $$files($$IPP_FILES)

IPP_LIBRARY_COPIER.name = IPP library copier
IPP_LIBRARY_COPIER.input = IPP_FILES_LIST
IPP_LIBRARY_COPIER.output = $$top_builddir/${QMAKE_FILE_BASE}${QMAKE_FILE_EXT}
IPP_LIBRARY_COPIER.commands = $$QMAKE_COPY \
                              $$shell_quote($$shell_path(${QMAKE_FILE_IN})) \
                              $$shell_quote($$shell_path(${QMAKE_FILE_OUT}))
IPP_LIBRARY_COPIER.CONFIG += no_link target_predeps

QMAKE_EXTRA_COMPILERS += IPP_LIBRARY_COPIER
